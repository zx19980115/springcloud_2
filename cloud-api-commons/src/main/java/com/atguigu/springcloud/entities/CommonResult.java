package com.atguigu.springcloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResult<T> {
    //一般返回给前端的是404 not_found这种格式的
    private Integer code;
    private String message;
    private  T     data;

    public static void main(String[] args) {

    }
    public  CommonResult(Integer code,String message){
        this(code, message, null);
    }
}
